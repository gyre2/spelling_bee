import pyttsx3 as pt
import random
import time

def speak(w, r=100, vdelta=0):
    engine.setProperty('rate', r)
    engine.setProperty('volume', 0.75+vdelta)
    engine.say(w)
    engine.runAndWait()

def quiz(wordlist):
    done = []
    while len(done) != len(wordlist):
        todo = int
        while True:
            todo = random.randint(0, len(wordlist)-1)
            if todo not in done:
                break

        print('Ready?  Listen:')
        speak(wordlist[todo])
        attempt = input('Please spell that word:')

        if attempt.lower() == wordlist[todo]:
            praiseword = random.choice(success_words)
            print(praiseword)
            speak(praiseword, 200)
            done.append(todo)
            print('Spelled %s out of %s words correctly' % (str(len(done)), str(len(wordlist))))
            print()
        else:
            speak('Incorrect. Please watch the correct spelling:', 200, 0.25)
            print(wordlist[todo])
            print()
            training = ''
            while training != wordlist[todo]:
                training = input('Please type the word correctly:')
            print('Good... let\'s move on')
            print()
    print()

engine = pt.init()
engine.setProperty('rate', 100)
success_words = ['great', 'very good', 'very very good', 'spectacular', 'phenomenal', 'smashing', 'brilliant', 'awesome', 'top drawer', 'spot on', 'groovy', 'not baaaaad', 'I like it']
week4_words = ["twelve","proverb","across","today","during","metaphor","simile","quiz","pupil","proposal","bridal","dental","gerbil","nasal","postal","hemisphere","immigrant","passage","population","pioneer"]
quiz(week4_words)
print('ALL DONE!')
speak('YAY!')